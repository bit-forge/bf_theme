<?php

use Drupal\Core\Render\Element;
use Drupal\Core\Template\Attribute;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_preprocess_html().
 */
function bf_theme_preprocess_html(&$variables) {
  $path = \Drupal::service('path.current')->getPath();
  $alias = trim(\Drupal::service('path_alias.manager')
    ->getAliasByPath($path), '/');

  if ( !empty($variables['page']['sidebar_first']) || !empty($variables['page']['sidebar_second'])) {
    $bf_layout_library = \Drupal::service('library.discovery')->getLibraryByName('bf_layouts', 'layout_sidebars');

    if ( $bf_layout_library ) {
      $variables['page']['#attached']['library'][] = 'bf_layouts/layout_sidebars';
    }
  }

  // Alias path class.
  $alias_class = preg_replace("/\//", '-', $alias);
  if (!empty($alias_class) && strpos($alias_class, 'node') !== 0) {
    $variables['attributes']['class'][] = Html::cleanCssIdentifier('alias--' . $alias_class);
  }
  // If is homepage.
  $variables['attributes']['class'][] = \Drupal::service('path.matcher')
    ->isFrontPage() ? 'frontpage' : '';
  // Node type class.
  $variables['attributes']['class'][] = isset($variables['node_type']) ? 'nodetype--' . $variables['node_type'] : '';
  // Logged in class.
  $variables['attributes']['class'][] = $variables['logged_in'] ? 'logged-in' : 'logged-out';

  $system_path = \Drupal::service('path.current')->getPath();
  $page_path = explode('/', $system_path)[1];
  if (!empty($page_path)) {
    $variables['attributes']['class'][] = Html::cleanCssIdentifier('path--' . $page_path);
  }

  $is_admin = \Drupal::service('router.admin_context')->isAdminRoute();
  if ( $is_admin ) {
    $variables['attributes']['class'][] = Html::cleanCssIdentifier('route--admin');
  }
}

/**
 * Implements hook_preprocess_page().
 */
function bf_theme_preprocess_page(&$variables) {
  $variables['logo'] = theme_get_setting('logo.url');

  $variables['main_attributes'] = new Attribute(['class' => []]);

  if ( !empty($variables['page']['sidebar_first']) || !empty($variables['page']['sidebar_second'])) {
    $variables['main_attributes']['class'] = ['content', 'layout--sidebars'];

    if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
      $variables['main_attributes']['class'][] = 'layout--sidebars--both';
    }
    elseif (!empty($variables['page']['sidebar_first'])) {
      $variables['main_attributes']['class'][] = 'layout--sidebars--first';
    }
    elseif (!empty($variables['page']['sidebar_second'])) {
      $variables['main_attributes']['class'][] = 'layout--sidebars--second';
    }
  }

  $current_path = \Drupal::service('path.current')->getPath();

  if ( in_array(explode('/', $current_path)[1], ['webform', 'user']) ) {
    $variables['main_attributes']['class'][] = 'layout-container--page-width';
  }
}

/**
 * Implements template_preprocess_block().
 */
function bf_theme_preprocess_block(&$variables) {
  // Custom block type helper classes.
  if (isset($variables['elements']['content']['#block_content'])) {
    $bundle = $variables['elements']['content']['#block_content']->bundle();
    $bundle_class = str_replace('_', '-', $bundle);
    if (isset($variables['attributes']['class'])) {
      $variables['attributes']['class'][] = Html::cleanCssIdentifier('block--bundle-' . $bundle_class);
      $variables['attributes']['data-bundle-class'] = $bundle_class;
    }
  }

  if ( $variables['elements']['#plugin_id'] == 'local_tasks_block' ) {
    foreach($variables['content']['#primary'] as &$task) {
      $task['#attributes']['class'] = 'tabs__tab';
      $task['#link']['localized_options']['attributes']['class'][] = 'tabs__tab-link';
    }
    $variables['content']['#primary']['#attributes'] = new Attribute(['class' => ['tabs']]);
  }
}

/**
 * Implements hook_preprocess_node().
 */
function bf_theme_preprocess_node(&$variables) {
  // Helper variables for multiple nodes.
  if (!empty($variables['elements']['#entity_type'])) {
    $variables['attributes']['class'][] = Html::cleanCssIdentifier('entity--type--' . $variables['elements']['#entity_type']);
  }
}

/**
 * Implements hook_preprocess_media().
 */
function bf_theme_preprocess_media(&$variables) {
  $variables['attributes']['class'][] = Drupal\Component\Utility\Html::cleanCssIdentifier($variables['view_mode']);
}

function bf_theme_preprocess_field(&$variables) {
  foreach ( Element::children($variables['element']) as $delta ) {
    if ( isset($variables['element'][$delta]['#item_attributes']) ) {
      $variables['items'][$delta]['attributes']->merge(new Attribute($variables['element'][$delta]['#item_attributes']));
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for \Drupal\node\NodeForm.
 *
 * Changes vertical tabs to container and adds meta information.
 * Code borrowed from Seven theme.
 */
function bf_theme_form_node_form_alter(&$form, FormStateInterface $form_state) {
  $node = $form_state->getFormObject()->getEntity();
  $form['#theme'] = ['node_edit_form'];
  $form['advanced']['#type'] = 'container';
  $is_new = !$node->isNew() ? \Drupal::service('date.formatter')->format($node->getChangedTime(), 'short') : t('Not saved yet');
  $form['meta'] = [
    '#attributes' => ['class' => ['entity-meta__header']],
    '#type'       => 'container',
    '#group'      => 'advanced',
    '#weight'     => -100,
    'published'   => [
      '#type'       => 'html_tag',
      '#tag'        => 'h3',
      '#value'      => $node->isPublished() ? t('Published') : t('Not published'),
      '#access'     => !$node->isNew(),
      '#attributes' => [
        'class' => 'entity-meta__title',
      ],
    ],
    'changed'     => [
      '#type'               => 'item',
      '#wrapper_attributes' => [
        'class' => [
          'entity-meta__last-saved',
          'container-inline',
        ],
      ],
      '#markup'             => '<h4 class="label inline">' . t('Last saved') . '</h4> ' . $is_new,
    ],
    'author'      => [
      '#type'               => 'item',
      '#wrapper_attributes' => [
        'class' => [
          'author',
          'container-inline',
        ],
      ],
      '#markup'             => '<h4 class="label inline">' . t('Author') . '</h4> ' . $node->getOwner()
        ->getUsername(),
    ],
  ];
  $form['revision_information']['#type'] = 'container';
  $form['revision_information']['#group'] = 'meta';
}

/**
 * Implements hook_preprocess_menu().
 *
 * Add custom classes to main menu.
 */
function bf_theme_preprocess_menu(&$variables, $hook) {
  if ( isset($variables['menu_name']) && in_array($variables['menu_name'], ['main', 'secondary']) ) {
    $variables['attributes']['class'][] = 'navbar';

    foreach ( $variables['items'] as $item ) {
      _set_menu_link_options($item);
    }
  }
}

function _set_menu_link_options(&$item) {
  $url = &$item['url'];
  $options = $url->getOptions();

  if ( !isset($options['attributes']) ) {
    $options['attributes'] = [];
  }

  if ( !isset($options['attributes']['class']) ) {
    $options['attributes']['class'] = [];
  }

  if ( is_array($options['attributes']['class']) ) {
    $options['attributes']['class'][] = 'menu__link';
  } else {
    $options['attributes']['class'] .= ' menu__link';
    $options['attributes']['class'] = ltrim($options['attributes']['class']);
  }

  $url->setOptions($options);

  if ( !empty($item['below']) ) {
    $item['attributes']->addClass('is-expanded');

    foreach ( $item['below'] as $item ) {
      _set_menu_link_options($item);
    }
  }
}

function bf_theme_preprocess_views_view(&$variables) {
  //$variables['content_attributes'] = new Attribute(['class' => []]);
}
