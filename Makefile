init:
	cd node_modules/bf-theme-tools; ./setup.sh;\

dev: gulp-dev

prod: gulp-prod

gulp-prod:
	cd node_modules/bf-theme-tools; gulp;\

gulp-dev:
	cd node_modules/bf-theme-tools; gulp watch;\

new-sass-component:
	$(shell mkdir -p assets/sass/$(parent)/$(name); touch assets/sass/$(parent)/$(name)/_$(name).scss; echo "@import '$(parent)/$(name)/$(name)';" >> assets/sass/styles.scss;)
	@echo "new sass component created"
