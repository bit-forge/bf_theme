
/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function (Drupal, once, document) {

  'use strict';

  Drupal.behaviors.mobile_menu = {
    attach: function (context, settings) {
      const elements = once('js-mobile-menu', '#block-mainnavigation', context);

      elements.forEach((element, index) => {
        let menuNav = document.createElement('nav');
        menuNav.setAttribute('id', 'mmenu');
        menuNav.classList.add('mm');

        let menuClone = element.querySelector('.menu').cloneNode(true);

        menuNav.append(menuClone);
        context.querySelector('body').prepend(menuNav);

        var menu = new MmenuLight(context.getElementById('mmenu'), 'all');

        var navigator = menu.navigation({
          title: '',
          theme: 'light',
          selected: 'Selected'
        });

        var drawer = menu.offcanvas();

        context.querySelector('#mobile-menu-trigger .menu__link').addEventListener('click', function handleMobileLinkTriggerClick() {
          if ( document.getElementById('mmenu').classList.contains('opened') ) {
            drawer.close();
          } else {
            drawer.open();
          }
        });
      });
    }
  };

})(Drupal, once, document);
